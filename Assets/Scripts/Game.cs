using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Game : MonoBehaviour
{
    IGameInput input;
    public PlayerController Player;
    public Camera Cam;
    public float PlayerTrackingMinX = -3.0f;
    public float PlayerTrackingMaxX;

    public Transform Goal;

    public GameObject ButtonRetry;
    public GameObject ButtonNext;
    public GameObject TextWon;
    public GameObject TextLost;
    public GameObject TextPaused;
    public Text TextHealth;
    public Text TextTime;

    public float totalTime = 15.0f;
    public float timeLeft;

    bool isPaused;
    float camDeltaX;
    bool gameOver;

    void Awake()
    {
        input = new GameInputSimpleKeyboard();
        AssignPlayer();
        AssignCamera();
        StartCoroutine(Countdown());
        timeLeft = totalTime;
        PlayerTrackingMaxX = Goal.transform.position.x - 3.0f;
    }

    IEnumerator Countdown()
    {
        while (!isPaused && timeLeft > 0)
        {
            yield return new WaitForSeconds(1.0f);
            timeLeft--;
        }
        // ����� �����!
        if (timeLeft <= 0)
            Lose();
    }

    void AssignPlayer()
    {
        if (Player == null)
            Player = FindObjectOfType<PlayerController>();
        if (Player == null)
            ErrorMissingComponent("player");
    }

    void AssignCamera()
    {
        if (Cam == null)
            Cam = Camera.main;
        if (Cam == null)
            ErrorMissingComponent("camera");

        camDeltaX = Cam.transform.position.x - PlayerTrackingMinX;
    }

    void ErrorMissingComponent(string label)
    {
        throw new Exception($"Couldn't find a {label} object on this level. Are you sure it exists in the scene {SceneManager.GetActiveScene().name}");
    }

    void Update()
    {
        if (input.IsPausePressed())
            TogglePause();

        if (isPaused || gameOver)
            return;

        TextHealth.text = $"Health: {(int)Player.Health}";
        int minutes = Mathf.FloorToInt(timeLeft / 60);
        int seconds = Mathf.FloorToInt(timeLeft % 60);
        TextTime.text = $"Remained time: {string.Format("{0:00}:{1:00}", minutes, seconds)}";
        Player.Move(input.GetMovementDirection(), input.IsJumpPressed(), input.IsCrouchPressed());

        if (Player.transform.position.x > Goal.transform.position.x)
            Win();
        else if (Player.Health < 0)
            Lose();
    }

    //Making camera trail the player in LateUpdate because player's new position is ready by then
    void LateUpdate()
    {
        CameraUpdateTrailing();
    }

    void CameraUpdateTrailing()
    {
        float playerX = Player.transform.position.x;
        if (playerX < PlayerTrackingMinX || playerX > PlayerTrackingMaxX)
            return;

        var camTfm = Cam.transform;
        var camPos = camTfm.position;

        camPos.x = playerX + camDeltaX;

        camTfm.position = camPos;
    }

    void TogglePause()
    {
        SetPause(!isPaused);
    }

    void SetPause(bool pause)
    {
        PauseGame(pause);
        TextPaused.SetActive(pause);
    }

    void PauseGame(bool pause)
    {
        Time.timeScale = pause ? 0.0f : 1.0f;
        isPaused = pause;
    }

    void Win()
    {
        GameOver();
        TextWon.SetActive(true);
        ButtonRetry.SetActive(true);
        if (canLoadNextScene()) ButtonNext.SetActive(true);
    }

    public void NextLevel()
    {
        LoadNextScene();
        PauseGame(false);
    }

    void LoadNextScene()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        int nextSceneIndex = currentSceneIndex + 1;

        if (canLoadNextScene())
        {
            SceneManager.LoadScene(nextSceneIndex);
        }
        else
        {
            Debug.Log("��������� ����� ������ ����.");
        }
    }

    bool canLoadNextScene()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        int nextSceneIndex = currentSceneIndex + 1;

        return nextSceneIndex < SceneManager.sceneCountInBuildSettings;
    }

    void Lose()
    {
        TextTime.text = $"Remained time: 00:00";
        GameOver();
        TextLost.SetActive(true);
        ButtonRetry.SetActive(true);
    }

    void GameOver()
    {
        gameOver = true;
        PauseGame(true);
    }

    public void RestartLevel()
    {
        PauseGame(false);
        var level = SceneManager.GetActiveScene().name;
        SceneManager.LoadScene(level);
    }
}
